module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    'nativewind/babel',
    [
      'module-resolver',
      {
        alias: {
          // This needs to be mirrored in tsconfig.json
          components: './src/component',
          screen: './src/screen',
          hooks: './src/hooks',
          images: './src/images',
          helpers: './src/helpers',
        },
      },
    ],
  ],
};
/**
 * 
 * components/*": ["src/component/index"],
      "screen/*": ["src/screen/index"],
      "hooks/*": ["src/hooks/index"],
      "images/*": ["src/images/index"],
      "helpers/*": ["src/helpers/index"]
 */
