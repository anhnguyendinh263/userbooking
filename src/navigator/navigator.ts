// import {BottomTabScreenProps} from '@react-navigation/bottom-tabs';
// import {CompositeScreenProps} from '@react-navigation/native';
// import {StackScreenProps} from '@react-navigation/stack';
declare global {
  namespace ReactNavigation {
    interface RootParamList extends RootStackParamsList {}
  }
}

export type RootStackParamsList = {
  intro: undefined;
  authOptions: undefined;
  login: undefined;
  register: undefined;
  home: undefined;
  profile: undefined;
  detailRestaurant: undefined;
  dishDetail: undefined;
  forgotPassword: undefined;
  otp: undefined;
  offters: undefined;
  chatList: undefined;
  chatDetail: undefined;
  myBooking: undefined;
  myWallet: undefined;
};

export enum Routes {
  intro = 'intro',
  authOptions = 'authOptions',
}

// export type RootStackScreenProps<Screen extends keyof RootStackParamsList> =
//   StackScreenProps<RootStackParamsList, Screen>;

// export type RootTabParamList = {};

// export type RootTabScreenProps<Screen extends keyof RootTabParamList> =
//   CompositeScreenProps<
//     BottomTabScreenProps<RootTabParamList, Screen>,
//     StackScreenProps<RootStackParamsList>
//   >;The expected type comes from property 'name' which is declared here on type
//  'IntrinsicAttributes & RouteConfig<RootStackParamsList, keyof RootStackParamsList, StackNavigationState<ParamListBase>,
//   NativeStackNavigationOptions, NativeStackNavigationEventMap>'
