import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {RootStackParamsList, Routes} from './navigator';

import React from 'react';
import {IntroFoodApp, AuthOptions} from '../screen';
const Stack = createNativeStackNavigator<RootStackParamsList>();
const NavigatorApp = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="intro"
      component={IntroFoodApp}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="authOptions"
      component={AuthOptions}
      options={{headerShown: false}}
    />
  </Stack.Navigator>
);

export default NavigatorApp;
