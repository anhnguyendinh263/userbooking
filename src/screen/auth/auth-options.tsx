import React from 'react';

import {ButtonBooking} from 'components/button';
import {View} from 'components/view';
import {TextBooking} from 'components/text';
export const AuthOptions = () => {
  return (
    <View>
      <View className="mt-10 mx-10">
        <ButtonBooking title="Login with facebook" />
        <TextBooking content="content" />
      </View>
    </View>
  );
};
