import React, {useEffect, useMemo, useRef} from 'react';
import {
  Button,
  Dimensions,
  Image,
  ListRenderItemInfo,
  Text,
  View,
  ViewStyle,
} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import styles from './styles';
import {RootStackParamsList, Routes} from '../../navigator/navigator';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
export const IntroFoodApp = ({
  navigation,
}: NativeStackScreenProps<RootStackParamsList, 'intro'>) => {
  const slides = [
    {
      key: 1,
      text: 'Description.\nSay something cool',
      image: require('../../images/img/intro1.jpg'),
      //   backgroundColor: '#59b2ab',
    },
    {
      key: 2,
      text: 'Other cool stuff',
      image: require('../../images/img/intro2.png'),
      backgroundColor: '#febe29',
    },
    {
      key: 3,
      text: "I'm already out of descriptions\n\nLorem ipsum bla bla bla",
      image: require('../../images/img/intro3.png'),
      backgroundColor: '#22bcb5',
    },
    {
      key: 4,
      text: "I'm already out of descriptions\n\nLorem ipsum bla bla bla",
      image: require('../../images/img/intro4.png'),
      backgroundColor: '#22bcb5',
    },
  ];
  const {width: screenW, height: screenH} = Dimensions.get('window');
  const introRef = useRef<AppIntroSlider>(null);

  const activeIndex = useMemo(
    () => introRef?.current?.state?.activeIndex === 0,
    [introRef?.current?.state?.activeIndex],
  );

  const activeDotStyle = useMemo(
    () => (!activeIndex ? ({backgroundColor: 'transparent'} as ViewStyle) : {}),
    [activeIndex],
  );

  useEffect(() => {
    if (introRef?.current?.state?.activeIndex === 0) {
      var timeOutFirstSlice = setTimeout(() => {
        introRef?.current?.goToSlide(1);
      }, 10000);
    }
    return () => {
      clearTimeout(timeOutFirstSlice);
    };
  }, []);

  const renderItem = ({item}: ListRenderItemInfo<any>) => {
    return (
      <View style={styles.wrapperIntro}>
        <View style={styles.contentImage}>
          <Image
            source={item.image}
            style={[
              styles.image,
              {
                width: 'auto',
                height: screenH,
              },
            ]}
            resizeMode={'contain'}
          />
        </View>
        <View style={styles.viewContent}>
          <Text>{item.title}</Text>
          <Text>{item.subtitle}</Text>
        </View>
      </View>
    );
  };
  const handeRealApp = () => {
    navigation.navigate(Routes.authOptions);
    console.log('first real');
  };
  return (
    <AppIntroSlider
      ref={introRef}
      renderItem={renderItem}
      data={slides}
      onDone={() => {}}
      //   showPrevButton
      showNextButton={activeIndex}
      activeDotStyle={activeDotStyle}
      bottomButton={activeIndex}
      renderDoneButton={() => {
        return <Button title="Done" onPress={handeRealApp} />;
      }}
      renderNextButton={() => {
        return <Button title="Next" />;
      }}
      // renderPrevButton={() => {
      //   return <Button title="Prev" />;
      // }}
      // renderSkipButton={() => {
      //   return <Button title="Skip" />;
      // }}
    />
  );
};
