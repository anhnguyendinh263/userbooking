import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  textWhite: {
    color: '#ffffff',
    fontSize: 15,
  },
  contentDot: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
  dot: {
    backgroundColor: 'red',
    width: 8,
    height: 8,
    marginRight: 8,
    borderRadius: 10,
  },
  dotActive: {
    backgroundColor: 'blue',
    width: 16,
  },
  wrapperIntro: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  image: {
    // height: '100%',
    alignSelf: 'center',
    aspectRatio: 343 / 400,
  },
  bgLotie: {flex: 1, backgroundColor: 'red'},
  contentImage: {
    flex: 1,
    marginBottom: 40,
    alignSelf: 'center',
  },
  viewContent: {
    flex: 0.9,
    paddingHorizontal: 30,
  },
  haflFlex: {
    // flex: 1 / 2,
  },
  nextButton: {},
  buttonContainer: {},
  pagination: {
    position: 'absolute',
    bottom: 40,
    paddingHorizontal: 24,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
  },
  btn: {
    paddingHorizontal: 20,
    borderRadius: 8,
  },
  btnSkip: {
    minWidth: 60,
    justifyContent: 'center',
  },
  btnRegister: {
    minWidth: 60,
  },
});
