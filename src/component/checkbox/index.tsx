import {PropsWithChildren, memo} from 'react';
import {View} from '../view';
import {CheckBox, CheckBoxProps as CheckBoxPropsRN} from '@rneui/themed';
import {Colors} from 'helpers/colors';
import {ViewStyle} from 'react-native';

// type InputProps = PropsWithChildren<{
//   title: string;
//   leftIcon?: any;
//   rightIcon?: any;
//   style?: string;
//   disabled: boolean;
//   errorMessage?: string;
//   label?: string;
//   placeholder: string;
//   onChangeText?: () => any;
// }>;
interface CheckBoxProps extends CheckBoxPropsRN {
  disable?: boolean;
  onChangeText?: () => any;
  checked: boolean;
  title: string;
  size?: number;
  style?: ViewStyle;
  onPress?: () => any;
}

export const CheckBoxBooking = memo((props: CheckBoxProps) => {
  const {
    title,
    checked,
    style,
    disabled,
    onChangeText,
    children,
    onPress,
    size,
    ...rest
  } = props;
  return (
    <CheckBox
      checkedColor={Colors.button}
      title={title}
      checked={checked}
      size={size}
      disabled={disabled}
      style={style}
      onPress={onPress}
      {...rest}
    />
  );
});
