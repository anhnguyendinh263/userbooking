import {PropsWithChildren, memo} from 'react';
import {View} from '../view';
import {Input, Icon, InputProps as InputPropsRN} from '@rneui/themed';

// type InputProps = PropsWithChildren<{
//   title: string;
//   leftIcon?: any;
//   rightIcon?: any;
//   style?: string;
//   disabled: boolean;
//   errorMessage?: string;
//   label?: string;
//   placeholder: string;
//   onChangeText?: () => any;
// }>;
interface InputProps extends InputPropsRN {
  title: string;
  leftIcon?: any;
  rightIcon?: any;
  disabled: boolean;
  errorMessage?: string;
  label?: string;
  placeholder: string;
  onChangeText?: () => any;
}

export const InputBooking = memo((props: InputProps) => {
  const {
    title,
    children,
    leftIcon,
    rightIcon,
    errorMessage,
    style,
    disabled,
    label,
    placeholder,
    onChangeText,
    ...rest
  } = props;
  return (
    <Input
      placeholder={placeholder}
      leftIcon={leftIcon}
      rightIcon={rightIcon}
      disabled={disabled}
      errorMessage={errorMessage}
      label={label}
      renderErrorMessage
      onChangeText={onChangeText}
      {...rest}
    />
  );
});
