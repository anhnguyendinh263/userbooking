import React, {useState} from 'react';
import {Switch} from '@rneui/themed';
import {View, Text, StyleSheet} from 'react-native';

type SwitchComponentProps = {
  value: boolean;
  onValueChange?: (value: boolean) => any;
};

export const SwitchComponent: React.FunctionComponent<SwitchComponentProps> = (
  props: SwitchComponentProps,
) => {
  const {value, onValueChange} = props;

  return (
    <View style={styles.view}>
      <Switch value={value} onValueChange={onValueChange} />
    </View>
  );
};

const styles = StyleSheet.create({
  view: {
    margin: 10,
  },
});

export default SwitchComponent;
