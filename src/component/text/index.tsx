import {PropsWithChildren, memo} from 'react';
import {View} from '../view';
import {Text, Icon, TextProps as TextPropsRN} from '@rneui/themed';

interface TextProps extends TextPropsRN {
  fontWeight?:
    | '100'
    | 'bold'
    | 'normal'
    | '200'
    | '300'
    | '400'
    | '500'
    | '600'
    | '700'
    | '800'
    | '900'
    | undefined;
  content: string;
  color?: string;
}

export const TextBooking = memo((props: TextProps) => {
  const {fontWeight, content, color, ...rest} = props;
  return (
    <Text
      style={{
        fontWeight: fontWeight,
        color: color,
      }}
      {...rest}>
      {content}
    </Text>
  );
});
