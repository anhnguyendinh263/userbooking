import {PropsWithChildren} from 'react';
import {View as ViewRn} from 'react-native';
import {twStyle} from '../style';
type ViewProps = PropsWithChildren<{
  className?: string;
}>;

export const View = ({children, className, ...rest}: ViewProps) => {
  return <ViewRn style={className && twStyle`${className}`} {...rest} />;
};
