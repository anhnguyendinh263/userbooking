import {Button, Text} from '@rneui/themed';
import {PropsWithChildren} from 'react';
import {StyleProp, TextStyle} from 'react-native';
import {View} from '../view';
import {Colors} from '../../helpers/colors';

type ButtonProps = PropsWithChildren<{
  title: string;
  icon?: any;
  loading?: boolean;
  style?: string;
  textStyle?: StyleProp<TextStyle>;
  disabled?: boolean;
  size?: 'md' | 'lg' | 'sm';
  onPress?: () => any;
}>;

export const ButtonBooking = (props: ButtonProps) => {
  const {
    title,
    children,
    loading,
    icon,
    size,
    style,
    textStyle,
    disabled,
    onPress,
    ...rest
  } = props;
  return (
    <View className={style}>
      <Button
        color={Colors.button}
        icon={icon}
        loading={loading}
        size={size}
        disabled={disabled}
        onPress={onPress}
        {...rest}>
        <Text style={textStyle}>{title}</Text>
      </Button>
    </View>
  );
};
